//
//  ViewController.swift
//  мое приложение
//
//  Created by Daniil on 07.02.2019.
//  Copyright бла бла бла © 2019 Daniil. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Database.titles.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        if let myCell = cell as? MyCell {
            myCell.label.text =  Database.titles[indexPath.row]
            myCell.receiptImageView?.image = UIImage(named: Database.imageNames[indexPath.row])
        }
        
        return cell
   }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let nextController = storyboard?.instantiateViewController(withIdentifier: "ReceiptViewController") as? ReceiptViewController {
            nextController.receiptID = indexPath.row
            show(nextController, sender: nil)
        }
    }
}
