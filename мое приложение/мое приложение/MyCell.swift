//
//  MyCell.swift
//  мое приложение
//
//  Created by Daniil on 11.02.2019.
//  Copyright © 2019 Daniil. All rights reserved.
//

import UIKit

class MyCell: UITableViewCell {
    
    @IBOutlet var label: UILabel!
    @IBOutlet var receiptImageView: UIImageView!
    
}
