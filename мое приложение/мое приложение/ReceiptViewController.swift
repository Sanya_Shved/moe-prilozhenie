//
//  ReceiptViewController.swift
//  мое приложение
//
//  Created by Daniil on 04.03.2019.
//  Copyright © 2019 Daniil. All rights reserved.
//

import UIKit

class ReceiptViewController: UIViewController {

    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var cooking: UILabel!
    @IBOutlet var composition: UILabel!
    @IBOutlet var button: UIButton!
    
      var receiptID: Int = 0
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        photoImageView.image = UIImage(named: Database.imageNames[receiptID])
        nameLabel.text = Database.titles[receiptID]
        cooking.text = Database.cookings[receiptID]
        composition.text = Database.compositions[receiptID]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        let trueOrFalse:Bool
        trueOrFalse = true
        
        var myAge:Int
        myAge = 12
        
        let kg:Float
        kg = 2.16
        
       
        // комментарий для домашнего задания
        /* Зачем я пишу сдесь какую-то чушь? Я ведь знаю что мне попадет от Андрея Олеговича по пятое число */
    }
    
    func printInСonsole() {
        var catName:String
        catName = "Barsik"
        print(catName)
    }
    
    @IBAction func addToFavorite() {

        var lotReceipt: [Int] = []
        lotReceipt.append(receiptID)
        
        UserDefaults.standard.set(lotReceipt, forKey: "favorites0")
        UserDefaults.standard.synchronize()
        
        UserDefaults.standard.set(receiptID, forKey: "favorites")
        UserDefaults.standard.synchronize()
        print("\(receiptID) added to favorites")
    }

}
