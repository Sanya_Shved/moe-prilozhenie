////  FavoritesViewController.swift
//  мое приложение
//
//  Created by Daniil on 25.03.2019.
//  Copyright © 2019 Daniil. All rights reserved.
//



import UIKit

class FavoritesViewController: UITableViewController {
    
    var receiptIDs: [Int] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view, typically from a nib.
        readNumbers()
        tableView.reloadData()
    }
    
    func readNumbers() {
        receiptIDs = UserDefaults.standard.value(forKey: "favorites0") as! [Int]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return receiptIDs.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if let myCell = cell as? MyCell {
            let receiptID = receiptIDs[indexPath.row]
            myCell.label.text =  Database.titles[receiptID]
            let imageName: String
            imageName = Database.imageNames[receiptID]
            myCell.receiptImageView?.image = UIImage(named: imageName)
        }
        
        return cell
    }
    
   
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let nextController = storyboard?.instantiateViewController(withIdentifier: "ReceiptViewController") as? ReceiptViewController {
            nextController.receiptID = receiptIDs[indexPath.row]
            show(nextController, sender: nil)
        }
    }
}   
